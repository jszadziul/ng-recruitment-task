import config from './config';
import { EnvironmentEnum } from './constants/config.constants';

describe('Config', () => {
  it('should return regular config', () => {
    process.env.ENV = EnvironmentEnum.LOCAL;
    const configuration = config();
    expect(configuration.app.port).toEqual(3000);
  });

  it('should return test config', () => {
    process.env.ENV = EnvironmentEnum.TEST;
    const configuration = config();
    expect(configuration.app.port).toEqual(3020);
  });

  it('should return test config if ENV variable is not provided', () => {
    const configuration = config();
    expect(configuration.app.port).toEqual(3020);
  });
});
