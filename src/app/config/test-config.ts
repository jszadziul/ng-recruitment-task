import { IConfig } from './interface/config.interface';
import { EnvironmentEnum } from './constants/config.constants';
import {
  description as appDescription,
  name as appName,
  version as appVersion,
} from '../../../package.json';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';
import { typeormEntityRegistry } from '../modules/database/typeorm-entity.registry';

export const createTestConfig = (): IConfig => ({
  app: {
    name: appName,
    version: appVersion,
    description: appDescription,
    env: EnvironmentEnum.TEST,
    port: 3020,
    address: '0.0.0.0',
  },
  auth: {
    jwtSecret: 'secret',
    jwtIssuer: 'https://www.netguru.com/',
  },
  databaseConnection: {
    name: 'default' + new Date().valueOf(),
    type: 'postgres',
    host: process.env.TEST_DATABASE_HOST || 'localhost',
    port: 5432,
    username: 'postgrestest',
    password: 'testpass123',
    database: 'main',
    entities: typeormEntityRegistry,
    synchronize: true,
    namingStrategy: new SnakeNamingStrategy(),
    logging: false,
  },
  validationPipe: { whitelist: true, transform: true },
  omdb: {
    apiKey: 'testOmdbApiKey',
    apiAddress: 'http://www.omdbapi.com/',
  },
  basicMovieQuota: 5,
});
