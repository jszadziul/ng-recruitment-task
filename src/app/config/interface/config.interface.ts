import { EnvironmentEnum } from '../constants/config.constants';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { ValidationPipeOptions } from '@nestjs/common';

export interface IAppMetadata {
  name: string;
  version: string;
  description: string;
  env: EnvironmentEnum;
  port: number;
  address: string;
}

export interface IAuthConfig {
  jwtSecret: string;
  jwtIssuer: string;
}

export interface IOmdbConfig {
  apiKey: string;
  apiAddress: string;
}

export interface IConfig {
  app: IAppMetadata;
  auth: IAuthConfig;
  databaseConnection: TypeOrmModuleOptions;
  validationPipe: ValidationPipeOptions;
  omdb: IOmdbConfig;
  basicMovieQuota: number;
}
