export enum EnvironmentEnum {
  DEV = 'dev',
  LOCAL = 'local',
  TEST = 'test',
}
