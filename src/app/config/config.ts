import { IConfig } from './interface/config.interface';
import { EnvironmentEnum } from './constants/config.constants';
import {
  description as appDescription,
  name as appName,
  version as appVersion,
} from '../../../package.json';
import { createTestConfig } from './test-config';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';
import { typeormEntityRegistry } from '../modules/database/typeorm-entity.registry';

const createConfig = (): IConfig => ({
  app: {
    name: appName,
    version: appVersion,
    description: appDescription,
    env: process.env.ENV as EnvironmentEnum,
    port: parseInt(process.env.PORT) || 3000,
    address: '0.0.0.0',
  },
  auth: {
    jwtSecret: process.env.JWT_SECRET || 'secret',
    jwtIssuer: process.env.JWT_ISSUER || 'https://www.netguru.com/',
  },
  databaseConnection: {
    name: 'default' + new Date().valueOf(),
    type: 'postgres',
    host: process.env.DATABASE_HOST,
    port: parseInt(process.env.DATABASE_PORT) || 5432,
    username: process.env.DATABASE_USERNAME,
    password: process.env.DATABASE_PASSWORD,
    database: 'main',
    entities: typeormEntityRegistry,
    synchronize: true,
    namingStrategy: new SnakeNamingStrategy(),
    logging: true,
  },
  validationPipe: { whitelist: true, transform: true },
  omdb: {
    apiKey: process.env.OMDB_API_KEY,
    apiAddress: process.env.OMDB_API_ADDRESS,
  },
  basicMovieQuota: 5,
});

export default (): IConfig => {
  if (!process.env.ENV || process.env.ENV === EnvironmentEnum.TEST) {
    return createTestConfig();
  }
  return createConfig();
};
