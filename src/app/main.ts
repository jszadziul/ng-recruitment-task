/* istanbul ignore file */
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import { FastifyServerOptions } from 'fastify';
import * as qs from 'qs';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import {
  DocumentBuilder,
  SwaggerDocumentOptions,
  SwaggerModule,
} from '@nestjs/swagger';
import { IAppMetadata, IConfig } from './config/interface/config.interface';
import { ConfigService } from '@nestjs/config';

const fastifyServerConfig: FastifyServerOptions = {
  logger: true,
  querystringParser: (str) => qs.parse(str),
};

const bootstrap = async () => {
  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    new FastifyAdapter(fastifyServerConfig),
  );
  const configService = app.get<ConfigService<IConfig>>(ConfigService);
  app.useGlobalPipes(new ValidationPipe(configService.get('validationPipe')));
  const appMetadata = configService.get<IAppMetadata>('app');
  buildSwagger(app, '/api', appMetadata);
  const { port, address } = appMetadata;
  await app.listen(port, address);
};

const buildSwagger = (
  app: INestApplication,
  path: string,
  appMetadata: IAppMetadata,
  options?: SwaggerDocumentOptions,
): void => {
  const { name, version, description } = appMetadata;
  const config = new DocumentBuilder()
    .setTitle(name)
    .setVersion(version)
    .setDescription(description)
    .addBearerAuth()
    .build();
  const openApiDocument = SwaggerModule.createDocument(app, config, options);
  SwaggerModule.setup(path, app, openApiDocument);
};

void bootstrap();
