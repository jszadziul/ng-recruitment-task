import { Module } from '@nestjs/common';
import { ConfigurationModule } from './config/config.module';
import { MovieModule } from './modules/movie/movie.module';
import { AuthModule } from './modules/auth/auth.module';
import { DatabaseModule } from './modules/database/database.module';
import { HealthcheckModule } from './modules/healthcheck/healthcheck.module';

@Module({
  imports: [
    ConfigurationModule,
    MovieModule,
    AuthModule,
    DatabaseModule,
    HealthcheckModule,
  ],
})
export class AppModule {}
