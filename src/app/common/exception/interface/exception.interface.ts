import { ExceptionIdEnum } from '../constants/exception.constants';
import { HttpStatus } from '@nestjs/common';

export interface IBaseUnauthorizedException {
  statusCode: HttpStatus;
  error: string;
}
export interface IExtendedExceptionMessage {
  id: ExceptionIdEnum;
  message: string;
}
export interface IExceptionParams
  extends IBaseUnauthorizedException,
    IExtendedExceptionMessage {}
