import {
  BaseForbiddenException,
  BaseNotFoundException,
} from './base.exception';
import {
  ExceptionIdEnum,
  ExceptionMessageEnum,
} from './constants/exception.constants';

export class NotFoundMovieException extends BaseNotFoundException {
  constructor() {
    super({
      id: ExceptionIdEnum.MOVIE_NOT_FOUND,
      message: ExceptionMessageEnum.MOVIE_NOT_FOUND,
    });
  }
}

export class ForbiddenQuotaExceeded extends BaseForbiddenException {
  constructor() {
    super({
      id: ExceptionIdEnum.MONTHLY_QUOTA_EXCEEDED,
      message: ExceptionMessageEnum.MONTHLY_QUOTA_EXCEEDED,
    });
  }
}
