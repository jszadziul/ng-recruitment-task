import {
  ForbiddenQuotaExceeded,
  NotFoundMovieException,
} from '../movie.exception';
import { InternalServerErrorException } from '@nestjs/common';

export class ExceptionUtils {
  static throwForbiddenQuotaException() {
    throw new ForbiddenQuotaExceeded();
  }

  static throwNotFoundMovieException() {
    throw new NotFoundMovieException();
  }

  static throwInternalServerErrorException() {
    throw new InternalServerErrorException();
  }
}
