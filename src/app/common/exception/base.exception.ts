import {
  ForbiddenException,
  HttpStatus,
  NotFoundException,
} from '@nestjs/common';
import {
  IExceptionParams,
  IExtendedExceptionMessage,
} from './interface/exception.interface';
import { StandardErrorStringEnum } from './constants/exception.constants';

export class BaseNotFoundException extends NotFoundException {
  constructor(messageId: IExtendedExceptionMessage) {
    const exception: IExceptionParams = {
      statusCode: HttpStatus.NOT_FOUND,
      error: StandardErrorStringEnum.NOT_FOUND,
      ...messageId,
    };
    super(exception);
  }
}

export class BaseForbiddenException extends ForbiddenException {
  constructor(messageId: IExtendedExceptionMessage) {
    const exception: IExceptionParams = {
      statusCode: HttpStatus.FORBIDDEN,
      error: StandardErrorStringEnum.FORBIDDEN,
      ...messageId,
    };
    super(exception);
  }
}
