import { IExceptionParams } from '../interface/exception.interface';
import { HttpStatus } from '@nestjs/common';

export enum ExceptionIdEnum {
  MOVIE_NOT_FOUND = 'MOVIE_NOT_FOUND',
  MONTHLY_QUOTA_EXCEEDED = 'MONTHLY_QUOTA_EXCEEDED',
}

export enum ExceptionMessageEnum {
  MONTHLY_QUOTA_EXCEEDED = 'Monthly movies quota exceeded, to add more movies upgrade to premium.',
  MOVIE_NOT_FOUND = 'Movie not found',
}

export enum StandardErrorStringEnum {
  NOT_FOUND = 'Not Found',
  FORBIDDEN = 'Forbidden',
}

export const exampleMovieNotFound: IExceptionParams = {
  error: StandardErrorStringEnum.NOT_FOUND,
  id: ExceptionIdEnum.MOVIE_NOT_FOUND,
  message: ExceptionMessageEnum.MOVIE_NOT_FOUND,
  statusCode: HttpStatus.NOT_FOUND,
};

export const exampleQuotaExceededForbidden: IExceptionParams = {
  error: StandardErrorStringEnum.FORBIDDEN,
  id: ExceptionIdEnum.MONTHLY_QUOTA_EXCEEDED,
  message: ExceptionMessageEnum.MONTHLY_QUOTA_EXCEEDED,
  statusCode: HttpStatus.FORBIDDEN,
};
