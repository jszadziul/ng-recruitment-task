import { DeepPartial, FindConditions, Repository } from 'typeorm';

export abstract class BaseCrudService<T> {
  protected constructor(protected readonly repository: Repository<T>) {}

  createEntity(dto: DeepPartial<T>): T {
    return this.repository.create(dto);
  }

  async save(dto: DeepPartial<T>): Promise<T> {
    return this.repository.save(dto);
  }

  async findOne(params: FindConditions<T>): Promise<T> {
    return this.repository.findOne(params);
  }

  async find(params: FindConditions<T>): Promise<T[]> {
    return this.repository.find(params);
  }
}
