import { MovieEntity } from '../movie/entity/movie.entity';
import { UserMovieMonthlyQuotaEntity } from '../movie/entity/user-movie-quota.entity';

export const typeormEntityRegistry = [MovieEntity, UserMovieMonthlyQuotaEntity];
