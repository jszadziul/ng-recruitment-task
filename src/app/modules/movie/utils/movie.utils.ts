import { DeepPartial, UpdateResult } from 'typeorm';
import { ExceptionUtils } from '../../../common/exception/utils/exception.utils';
import { MovieEntity } from '../entity/movie.entity';
import { IOmdbMovie } from '../interface/movie.interface';

export class MovieUtils {
  static validateUserMovieQuotaUpdateResultOrThrow(updateResult: UpdateResult) {
    if (!updateResult.affected) {
      ExceptionUtils.throwForbiddenQuotaException();
    }
  }

  static mapOmdbMovieResponseToMovieEntity(
    omdbMovie: IOmdbMovie,
    userId: number,
  ): DeepPartial<MovieEntity> {
    const { Title, Released, Genre, Director } = omdbMovie;
    return {
      title: Title,
      released: Released,
      genre: Genre,
      director: Director,
      userId,
    };
  }
}
