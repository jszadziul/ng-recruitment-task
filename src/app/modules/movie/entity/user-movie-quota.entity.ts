import { Column, Entity, Index, Unique } from 'typeorm';
import { CommonEntity } from '../../../common/entity/common.entity';

@Entity('quota')
@Unique(['userId', 'year', 'month'])
export class UserMovieMonthlyQuotaEntity extends CommonEntity {
  @Index()
  @Column({ default: 0 })
  count: number;

  @Index()
  @Column()
  month: number;

  @Index()
  @Column()
  year: number;
}
