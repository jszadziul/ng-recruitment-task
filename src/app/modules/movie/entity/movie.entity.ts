import { Column, Entity } from 'typeorm';
import { CommonEntity } from '../../../common/entity/common.entity';

@Entity('movie')
export class MovieEntity extends CommonEntity {
  @Column()
  title: string;

  @Column()
  released: Date;

  @Column()
  genre: string;

  @Column()
  director: string;
}
