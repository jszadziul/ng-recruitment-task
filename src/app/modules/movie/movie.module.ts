import { HttpModule, Module } from '@nestjs/common';
import { MovieController } from './controller/movie.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MovieRepository } from './repository/movie.repository';
import { MovieService } from './service/movie.service';
import { UserMovieQuotaRepository } from './repository/user-movie-quota.repository';
import { UserMovieQuotaService } from './service/user-movie-quota.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([MovieRepository, UserMovieQuotaRepository]),
    HttpModule,
  ],
  controllers: [MovieController],
  providers: [MovieService, UserMovieQuotaService],
})
export class MovieModule {}
