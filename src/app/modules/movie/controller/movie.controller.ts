import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from '../../auth/guard/jwt.guard';
import { User } from '../../../common/decorator/user.decorator';
import { IUser } from '../../auth/interface/user.interface';
import {
  ApiBearerAuth,
  ApiForbiddenResponse,
  ApiNotFoundResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { MovieCreateDto } from '../dto/movie-create.dto';
import { MovieService } from '../service/movie.service';
import { MovieEntity } from '../entity/movie.entity';
import {
  exampleMovieNotFound,
  exampleQuotaExceededForbidden,
} from '../../../common/exception/constants/exception.constants';

@ApiTags('Movies')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('movies')
export class MovieController {
  constructor(protected readonly movieService: MovieService) {}

  @ApiNotFoundResponse({
    schema: { example: exampleMovieNotFound },
  })
  @ApiForbiddenResponse({
    schema: { example: exampleQuotaExceededForbidden },
  })
  @ApiOperation({
    description:
      'Fetch movie additional movie details based on title and save them in database.',
  })
  @Post()
  async createMovie(
    @Body() movieCreateDto: MovieCreateDto,
    @User() user: IUser,
  ): Promise<MovieEntity> {
    return this.movieService.saveOne(movieCreateDto, user);
  }

  @ApiOperation({
    description: 'Fetch all movies saved by user.',
  })
  @Get()
  async getMovies(@User() user: IUser): Promise<MovieEntity[]> {
    return this.movieService.getMany(user);
  }
}
