import { ConfigService } from '@nestjs/config';
import { IConfig } from '../../../config/interface/config.interface';
import { UserMovieQuotaRepository } from '../repository/user-movie-quota.repository';
import { Injectable } from '@nestjs/common';
import { IUserMovieQuotaFindParams } from '../interface/movie.interface';
import { BaseCrudService } from '../../../common/service/base.service';
import { UserMovieMonthlyQuotaEntity } from '../entity/user-movie-quota.entity';
import { QueryRunner, UpdateResult } from 'typeorm';
import { ExceptionUtils } from '../../../common/exception/utils/exception.utils';

@Injectable()
export class UserMovieQuotaService extends BaseCrudService<UserMovieMonthlyQuotaEntity> {
  protected readonly quotaCount: number;

  constructor(
    protected readonly userMovieQuotaRepository: UserMovieQuotaRepository,
    protected readonly configService: ConfigService<IConfig>,
  ) {
    super(userMovieQuotaRepository);
    this.quotaCount = configService.get('basicMovieQuota');
  }

  async checkUserMovieQuota(
    userId: number,
  ): Promise<IUserMovieQuotaFindParams> {
    const currentDate = new Date();
    const year = currentDate.getUTCFullYear();
    const month = currentDate.getUTCMonth() + 1;

    const userQuotaFindParams: IUserMovieQuotaFindParams = {
      year,
      month,
      userId,
    };
    const userQuotaEntity = await this.findOne(userQuotaFindParams);
    userQuotaEntity
      ? userQuotaEntity.count >= this.quotaCount
        ? ExceptionUtils.throwForbiddenQuotaException()
        : userQuotaEntity
      : await this.save(userQuotaFindParams);

    return userQuotaFindParams;
  }

  async incrementUserMovieQuotaCounter(
    params: IUserMovieQuotaFindParams,
    queryRunner: QueryRunner,
  ): Promise<UpdateResult> {
    return queryRunner.manager
      .getCustomRepository(UserMovieQuotaRepository)
      .incrementUserMovieQuotaCounter({ ...params, count: this.quotaCount });
  }
}
