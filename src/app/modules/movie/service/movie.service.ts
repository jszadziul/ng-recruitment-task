import { HttpService, Injectable } from '@nestjs/common';
import { MovieRepository } from '../repository/movie.repository';
import { MovieCreateDto } from '../dto/movie-create.dto';
import { ConfigService } from '@nestjs/config';
import {
  IConfig,
  IOmdbConfig,
} from '../../../config/interface/config.interface';
import { IGetMovieQueryParams, IOmdbMovie } from '../interface/movie.interface';
import { IUser } from '../../auth/interface/user.interface';
import { MovieEntity } from '../entity/movie.entity';
import { UserMovieQuotaService } from './user-movie-quota.service';
import { RolesEnum } from '../../auth/constants/auth.constants';
import { BaseCrudService } from '../../../common/service/base.service';
import { Connection } from 'typeorm';
import { ExceptionUtils } from '../../../common/exception/utils/exception.utils';
import { MovieUtils } from '../utils/movie.utils';
import { ForbiddenQuotaExceeded } from '../../../common/exception/movie.exception';

@Injectable()
export class MovieService extends BaseCrudService<MovieEntity> {
  protected readonly omdbConfig: IOmdbConfig;

  constructor(
    protected readonly movieRepository: MovieRepository,
    protected readonly httpService: HttpService,
    protected readonly configService: ConfigService<IConfig>,
    protected readonly userMovieQuotaService: UserMovieQuotaService,
    protected readonly connection: Connection,
  ) {
    super(movieRepository);
    this.omdbConfig = configService.get('omdb');
  }

  async saveOne(
    movieCreateDto: MovieCreateDto,
    user: IUser,
  ): Promise<MovieEntity> {
    const { userId, role } = user;
    const basicUserQuotaFindParams =
      role === RolesEnum.BASIC
        ? await this.userMovieQuotaService.checkUserMovieQuota(userId)
        : undefined;

    const omdbMovie = await this.fetchMovieDataFromOmdb(movieCreateDto);
    const movieEntity = this.createEntity(
      MovieUtils.mapOmdbMovieResponseToMovieEntity(omdbMovie, userId),
    );

    const queryRunner = this.connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();
    let createdMovie: MovieEntity;
    try {
      createdMovie = await queryRunner.manager.save(movieEntity);
      if (basicUserQuotaFindParams) {
        const updateResult =
          await this.userMovieQuotaService.incrementUserMovieQuotaCounter(
            basicUserQuotaFindParams,
            queryRunner,
          );
        MovieUtils.validateUserMovieQuotaUpdateResultOrThrow(updateResult);
      }
      await queryRunner.commitTransaction();
    } catch (e) {
      await queryRunner.rollbackTransaction();
      e instanceof ForbiddenQuotaExceeded
        ? ExceptionUtils.throwForbiddenQuotaException()
        : ExceptionUtils.throwInternalServerErrorException();
    } finally {
      await queryRunner.release();
    }

    return createdMovie;
  }

  async getMany(user: IUser): Promise<MovieEntity[]> {
    return this.find({ userId: user.userId });
  }

  protected async fetchMovieDataFromOmdb(
    movieCreateDto: MovieCreateDto,
  ): Promise<IOmdbMovie> {
    const { title } = movieCreateDto;
    const { apiKey, apiAddress } = this.omdbConfig;

    const queryParams: IGetMovieQueryParams = {
      apiKey,
      t: title,
    };
    const response = await this.httpService
      .get(apiAddress, {
        params: queryParams,
      })
      .toPromise();

    if (response.data.Response === 'False') {
      ExceptionUtils.throwNotFoundMovieException();
    }

    return response.data;
  }
}
