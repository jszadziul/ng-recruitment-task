import { EntityRepository, Repository } from 'typeorm';
import { UserMovieMonthlyQuotaEntity } from '../entity/user-movie-quota.entity';

@EntityRepository(UserMovieMonthlyQuotaEntity)
export class UserMovieQuotaRepository extends Repository<UserMovieMonthlyQuotaEntity> {
  async incrementUserMovieQuotaCounter(params: {
    userId: number;
    year: number;
    month: number;
    count: number;
  }) {
    const { userId, year, month, count } = params;
    return this.createQueryBuilder('quota')
      .update()
      .where('userId = :userId', { userId })
      .andWhere('year = :year', { year })
      .andWhere('month = :month', { month })
      .andWhere('count < :count', { count })
      .set({ count: () => 'count + 1', version: () => 'version + 1' })
      .execute();
  }
}
