export interface IGetMovieQueryParams {
  apiKey: string;
  t: string;
}

export interface IOmdbMovie {
  Title: string;
  Released: string;
  Genre: string;
  Director: string;
  Response: string;
}

export interface IUserMovieQuotaFindParams {
  year: number;
  month: number;
  userId: number;
}
