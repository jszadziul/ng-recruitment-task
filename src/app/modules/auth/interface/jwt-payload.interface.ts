import { IUser } from './user.interface';

export interface IJwtPayload extends IUser {
  iat: number;
  exp: number;
  iss: string;
  sub: string;
}
