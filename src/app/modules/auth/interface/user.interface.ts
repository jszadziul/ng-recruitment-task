export interface IUser {
  userId: number;
  name: string;
  role: string;
}
