import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {
  IAuthConfig,
  IConfig,
} from '../../../config/interface/config.interface';
import { IJwtPayload } from '../interface/jwt-payload.interface';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(protected readonly configService: ConfigService<IConfig>) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configService.get<IAuthConfig>('auth').jwtSecret,
      issuer: configService.get<IAuthConfig>('auth').jwtIssuer,
    });
  }

  validate(payload: IJwtPayload) {
    const { userId, name, role } = payload;

    if (!userId || !role) {
      throw new UnauthorizedException();
    }

    return { userId, name, role };
  }
}
