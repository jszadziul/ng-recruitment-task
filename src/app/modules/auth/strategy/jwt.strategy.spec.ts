import { Test } from '@nestjs/testing';
import { JwtStrategy } from './jwt.strategy';
import { UnauthorizedException } from '@nestjs/common';
import { AuthModule } from '../auth.module';
import { ConfigurationModule } from '../../../config/config.module';

describe('JwtStrategy', () => {
  let jwtStrategy: JwtStrategy;
  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [AuthModule, ConfigurationModule],
    }).compile();

    jwtStrategy = moduleRef.get(JwtStrategy);
  });

  describe('validate', () => {
    it.each([
      [undefined, 'a'],
      ['b', undefined],
      [undefined, undefined],
    ])(
      'should throw when token payload does not contain userId or role',
      (userId, role) => {
        expect(() =>
          jwtStrategy.validate({
            name: 'a',
            userId,
            role,
            exp: 12345456,
            iat: 12345451,
            iss: 'c',
            sub: 'd',
          } as any),
        ).toThrowError(UnauthorizedException);
      },
    );

    it('should return decoded jwt payload user information', () => {
      expect(
        jwtStrategy.validate({
          name: 'a',
          userId: 123,
          role: 'b',
          exp: 12345456,
          iat: 12345451,
          iss: 'c',
          sub: 'd',
        }),
      ).toMatchObject({
        userId: 123,
        name: 'a',
        role: 'b',
      });
    });
  });
});
