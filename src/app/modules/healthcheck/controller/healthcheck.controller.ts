import { Controller, Get } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { IUptime } from '../interface/uptime.interface';
import { HealthcheckService } from '../service/healthcheck.service';

@ApiTags('Healthcheck')
@Controller()
export class HealthcheckController {
  constructor(private readonly appService: HealthcheckService) {}

  @Get()
  healthCheck(): IUptime {
    return this.appService.getUptime();
  }
}
