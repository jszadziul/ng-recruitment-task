export interface IUptime {
  readonly uptimeInSec: number;
  readonly startTime: string;
  readonly version: string;
  readonly env: string;
}
