import { Injectable } from '@nestjs/common';
import { IUptime } from '../interface/uptime.interface';
import { ConfigService } from '@nestjs/config';
import {
  IAppMetadata,
  IConfig,
} from '../../../config/interface/config.interface';

@Injectable()
export class HealthcheckService {
  protected readonly startTime: Date;
  protected readonly appMetadata: IAppMetadata;

  constructor(protected readonly configService: ConfigService<IConfig>) {
    this.startTime = new Date();
    this.appMetadata = configService.get<IAppMetadata>('app');
  }

  getUptime(): IUptime {
    const now = new Date();
    const timeDiff = now.getTime() - this.startTime.getTime();
    return {
      uptimeInSec: timeDiff / 1000,
      startTime: `${this.startTime.toDateString()} ${this.startTime.toTimeString()}`,
      env: this.appMetadata.env,
      version: this.appMetadata.version,
    };
  }
}
