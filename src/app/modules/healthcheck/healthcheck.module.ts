import { Module } from '@nestjs/common';
import { HealthcheckService } from './service/healthcheck.service';
import { HealthcheckController } from './controller/healthcheck.controller';

@Module({
  providers: [HealthcheckService],
  controllers: [HealthcheckController],
})
export class HealthcheckModule {}
