import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { TestUtils } from '../../_utils/test.utils';
import { IUptime } from '../../../app/modules/healthcheck/interface/uptime.interface';
import { EnvironmentEnum } from '../../../app/config/constants/config.constants';

describe('HealthcheckController (e2e)', () => {
  let app: INestApplication;

  beforeAll(async () => {
    app = await TestUtils.InitTestApplication();
  });

  afterAll(async () => {
    await app.close();
  });

  it('/ (GET)', async (done) => {
    const { body } = await request(app.getHttpServer()).get('/').expect(200);

    expect(body).toMatchObject({
      uptimeInSec: expect.any(Number),
      startTime: expect.any(String),
      env: EnvironmentEnum.TEST,
      version: expect.any(String),
    } as IUptime);

    done();
  });
});
