import { HttpService, INestApplication } from '@nestjs/common';
import { TestUtils } from '../../_utils/test.utils';
import {
  basicUserMock,
  basicUserMockSecondary,
  premiumUserMock,
} from '../../_mocks/user.mock';
import { MovieRepository } from '../../../app/modules/movie/repository/movie.repository';
import {
  commonEntityMock,
  commonSerializedEntityMock,
} from '../../_mocks/entity.mock';
import { MovieEntity } from '../../../app/modules/movie/entity/movie.entity';
import { Connection } from 'typeorm';
import {
  omdbGetMovieNotFoundResponseMock,
  omdbGetMovieOkResponseMock,
} from '../../_mocks/omdb.mock';
import { UserMovieQuotaRepository } from '../../../app/modules/movie/repository/user-movie-quota.repository';
import { createMovieRequestWrapper } from '../../_utils/request.utils';
import { ExceptionIdEnum } from '../../../app/common/exception/constants/exception.constants';
import { httpServiceToPromiseMock } from '../../_mocks/service.mock';
import * as request from 'supertest';

describe('MovieController (e2e)', () => {
  let app: INestApplication;
  let basicUserAuthToken: { Authorization: string };
  let premiumUserAuthToken: { Authorization: string };
  let movieRepository: MovieRepository;
  let connection: Connection;
  let httpService: HttpService;

  beforeAll(async () => {
    app = await TestUtils.InitTestApplication();
    movieRepository = app.get(MovieRepository);
    connection = app.get(Connection);
    httpService = app.get(HttpService);
    httpService['get'] = jest.fn().mockImplementation(() => {
      return {
        toPromise: httpServiceToPromiseMock,
      };
    });
    basicUserAuthToken = TestUtils.getAuthHeaderWithSignedToken(basicUserMock);
    premiumUserAuthToken =
      TestUtils.getAuthHeaderWithSignedToken(premiumUserMock);
  });

  beforeEach(async () => {
    await TestUtils.ClearDatabase(connection);
  });

  afterAll(async () => {
    await app.close();
  });

  describe('/movies (POST)', () => {
    let quotaRepository;

    beforeAll(async () => {
      quotaRepository = app.get(UserMovieQuotaRepository);
    });

    it('should fetch additional movie details based on title provided by user and save to database', async (done) => {
      // ACT
      const { body } = await createMovieRequestWrapper(
        app,
        premiumUserAuthToken,
      ).expect(201);

      // ASSERT
      const userMovies = await movieRepository.find({
        userId: premiumUserMock.userId,
      });
      const { data } = omdbGetMovieOkResponseMock;
      expect(body).toMatchObject({
        title: data.Title,
        director: data.Director,
        genre: data.Genre,
        released: data.Released,
        ...commonSerializedEntityMock,
      } as unknown as MovieEntity);
      expect(userMovies.length).toEqual(1);
      expect(userMovies[0]).toMatchObject({
        director: expect.any(String),
        genre: expect.any(String),
        released: expect.any(Date),
        title: expect.any(String),
        ...commonEntityMock,
      } as MovieEntity);

      done();
    });

    it('it should increment movie quota counter for basic user', async (done) => {
      // ARRANGE
      const secondaryBasicUserToken = TestUtils.getAuthHeaderWithSignedToken(
        basicUserMockSecondary,
      );
      const date = new Date();
      const month = date.getUTCMonth() + 1;
      const year = date.getUTCFullYear();
      // Create exceeded previous calendar month quota to test monthly renewal of quotas
      await quotaRepository.save({
        userId: basicUserMock.userId,
        count: 5,
        month: month - 1,
        year,
      });
      for (let i = 0; i < 3; i++) {
        await createMovieRequestWrapper(app, basicUserAuthToken);
      }
      await createMovieRequestWrapper(app, premiumUserAuthToken);
      await createMovieRequestWrapper(app, secondaryBasicUserToken);

      // ACT // ASSERT
      const premiumUserQuota = await quotaRepository.find({
        userId: premiumUserMock.userId,
      });
      expect(premiumUserQuota.length).toEqual(0);

      const basicUserQuota = await quotaRepository.find({
        userId: basicUserMock.userId,
        month,
        year,
      });
      expect(basicUserQuota.length).toEqual(1);
      expect(basicUserQuota[0].count).toEqual(3);

      const secondaryBasicUserQuota = await quotaRepository.find({
        userId: basicUserMockSecondary.userId,
      });
      expect(secondaryBasicUserQuota.length).toEqual(1);
      expect(secondaryBasicUserQuota[0].count).toEqual(1);
      done();
    });

    it('it should throw ForbiddenQuotaExceeded when basic user exceeds monthly movie quota', async (done) => {
      // ARRANGE
      const date = new Date();
      const month = date.getUTCMonth() + 1;
      const year = date.getUTCFullYear();
      await quotaRepository.save({
        userId: basicUserMock.userId,
        count: 4,
        month,
        year,
      });
      await quotaRepository.save({
        userId: basicUserMock.userId,
        count: 3,
        month: month - 1,
        year,
      });

      // ACT
      await createMovieRequestWrapper(app, basicUserAuthToken).expect(201);

      const { body } = await createMovieRequestWrapper(
        app,
        basicUserAuthToken,
      ).expect(403);

      // ASSERT
      expect(body.id).toEqual(ExceptionIdEnum.MONTHLY_QUOTA_EXCEEDED);
      done();
    });

    it('it should throw NotFoundMovieException when movie not found in Omdb api', async (done) => {
      // ARRANGE
      httpServiceToPromiseMock.mockResolvedValueOnce(
        omdbGetMovieNotFoundResponseMock,
      );

      // ACT
      const { body } = await createMovieRequestWrapper(
        app,
        basicUserAuthToken,
      ).expect(404);

      // ASSERT
      expect(body.id).toEqual(ExceptionIdEnum.MOVIE_NOT_FOUND);
      done();
    });
  });
  describe('/movies (GET)', () => {
    it('should get all movies filtered per user', async () => {
      // ARRANGE
      const promises = [];
      for (let i = 0; i < 3; i++) {
        promises.push(createMovieRequestWrapper(app, premiumUserAuthToken));
      }
      const premiumUserRequestPromise = createMovieRequestWrapper(
        app,
        basicUserAuthToken,
      );
      await Promise.all([...promises, premiumUserRequestPromise]);

      // ACT
      const { body } = await request(app.getHttpServer())
        .get('/movies')
        .set(premiumUserAuthToken)
        .expect(200);

      // ASSERT
      const { data } = omdbGetMovieOkResponseMock;
      body.forEach((item) => {
        expect(item).toMatchObject({
          title: data.Title,
          director: data.Director,
          genre: data.Genre,
          released: expect.any(String),
          ...commonSerializedEntityMock,
        });
      });
      expect(body.length).toEqual(3);
    });
  });
});
