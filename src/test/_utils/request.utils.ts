import * as request from 'supertest';
import { MovieCreateDto } from '../../app/modules/movie/dto/movie-create.dto';
import { INestApplication } from '@nestjs/common';

export const createMovieRequestWrapper = (
  app: INestApplication,
  userToken: {
    Authorization: string;
  },
) => {
  return request(app.getHttpServer())
    .post('/movies')
    .set(userToken)
    .send({ title: 'Tenet' } as MovieCreateDto);
};
