import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import { Test } from '@nestjs/testing';
import { AppModule } from '../../app/app.module';
import * as jwt from 'jsonwebtoken';
import { IUser } from '../../app/modules/auth/interface/user.interface';
import { JwtSignOptions } from '@nestjs/jwt';
import { Connection } from 'typeorm';

export class TestUtils {
  static async InitTestApplication(): Promise<NestFastifyApplication> {
    const moduleRef = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    const app = moduleRef.createNestApplication<NestFastifyApplication>(
      new FastifyAdapter(),
    );

    await app.init();
    await app.getHttpAdapter().getInstance().ready();

    return app;
  }

  static async ClearDatabase(connection: Connection) {
    return connection.synchronize(true);
  }

  static getAuthHeaderWithSignedToken(
    user: IUser,
    secret = 'secret',
    options?: { expiresIn?: number; issuer?: string },
  ) {
    const defaultOptions: JwtSignOptions = {
      expiresIn: 30 * 60,
      issuer: 'https://www.netguru.com/',
    };
    const accessToken = jwt.sign(
      user,
      secret,
      Object.assign(defaultOptions, options),
    );

    return { Authorization: `Bearer ${accessToken}` };
  }
}
