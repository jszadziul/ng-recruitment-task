import { IOmdbMovie } from '../../app/modules/movie/interface/movie.interface';

export const omdbGetMovieOkResponseMock = {
  data: {
    Genre: 'Test Genre',
    Director: 'Test Director',
    Released: '03 Sep 2020',
    Title: 'Test Title',
    Response: 'True',
  } as IOmdbMovie,
};

export const omdbGetMovieNotFoundResponseMock = {
  data: {
    Response: 'False',
  } as IOmdbMovie,
};
