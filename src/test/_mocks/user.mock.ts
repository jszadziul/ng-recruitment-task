import { IUser } from '../../app/modules/auth/interface/user.interface';
import { RolesEnum } from '../../app/modules/auth/constants/auth.constants';

export const basicUserMock: IUser = {
  userId: 123,
  role: RolesEnum.BASIC,
  name: 'Basic Thomas',
};

export const basicUserMockSecondary: IUser = {
  userId: 12345,
  role: RolesEnum.BASIC,
  name: 'Basic Yorgen',
};

export const premiumUserMock: IUser = {
  userId: 434,
  role: RolesEnum.PREMIUM,
  name: 'Basic Thomas',
};
