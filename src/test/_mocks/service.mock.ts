import { omdbGetMovieOkResponseMock } from './omdb.mock';

export const httpServiceToPromiseMock = jest
  .fn()
  .mockResolvedValue(omdbGetMovieOkResponseMock);
