import { CommonEntity } from '../../app/common/entity/common.entity';

export const commonEntityMock: CommonEntity = {
  id: expect.any(Number),
  createdDate: expect.any(Date),
  updatedDate: expect.any(Date),
  userId: expect.any(Number),
  version: expect.any(Number),
};

export const commonSerializedEntityMock: CommonEntity = {
  id: expect.any(Number),
  createdDate: expect.any(String),
  updatedDate: expect.any(String),
  userId: expect.any(Number),
  version: expect.any(Number),
};
