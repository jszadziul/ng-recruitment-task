<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>

## Description

Movie API recruitment task. 

Frameworks/libraries/technologies of note: NestJS with Fastify http server, TypeORM and Postgres, Docker

## Running the app

1. To run the project you need to have docker and docker-compose installed on your machine.

2.  Create .env file in root dir of the project and copy contents of this [gitlab snippet](https://gitlab.com/jszadziul/ng-recruitment-task/-/snippets/2118745).

3. Run docker-compose (application will run on port 3001 by default: http://localhost:3001. You can change the port by modifying PORT variable in .env file).
```bash
$ docker-compose up
```
## User authorization

⚠️ To use the app you need to pass authorized user jwt token in Authorization header for each request.
```
Authorization: Bearer <token>
```
To get authorized user's token please run this microservice https://github.com/netguru/nodejs-recruitment-task (instructions in respective README.md)

⚠️ Please note that JWT_SECRET environment variable value must be the same in both microservices. In this project you can set value of JWT_TOKEN environmental variable in .env file.

## Test

To run e2e tests you need the app running (follow previous section), test database needs to be up and running.
```bash
# unit tests
$ npm run test:cov

# integration tests
$ npm run test:e2e
```

## API documentation

Once application is running, Swagger interactive documentation can be found at: http://localhost:3001/api